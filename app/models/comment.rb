class Comment < ActiveRecord::Base
  validates_presence_of :post_id, :body
end
